<?php

namespace Drupal\keycdn\Plugin\Purge\DiagnosticCheck;

use Drupal\keycdn\Entity\KeyCDNPurgerSettings;
use Drupal\purge\Plugin\Purge\DiagnosticCheck\DiagnosticCheckBase;
use Drupal\purge\Plugin\Purge\DiagnosticCheck\DiagnosticCheckInterface;
use Drupal\purge\Plugin\Purge\Purger\PurgersServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Verifies that only fully configured Key CDN purgers load.
 *
 * @PurgeDiagnosticCheck(
 *   id = "purge_purger_keycdn",
 *   title = @Translation("Key CDN "),
 *   description = @Translation("Tests your Key CDN configuration."),
 * )
 */
class ConfigurationCheck extends DiagnosticCheckBase implements DiagnosticCheckInterface {

  /**
   * The purgers service.
   */
  protected PurgersServiceInterface $purgePurgers;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->purgePurgers = $container->get('purge.purgers');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function run() {
    // Load configuration objects for all enabled HTTP purgers.
    $plugins = [];
    foreach ($this->purgePurgers->getPluginsEnabled() as $id => $plugin_id) {
      if (in_array($plugin_id, ['purge_purger_keycdn'])) {
        $plugins[$id] = KeyCDNPurgerSettings::load($id);
      }
    }

    // Perform checks against configuration.
    $labels = $this->purgePurgers->getLabels();
    foreach ($plugins as $id => $settings) {
      $t = ['@purger' => $labels[$id]];
      foreach (['zone', 'api_key'] as $f) {
        if (empty($settings->get($f))) {
          $this->recommendation = $this->t("@purger not configured.", $t);
          return self::SEVERITY_ERROR;
        }
      }
    }

    $this->recommendation = "All purgers configured.";
    return self::SEVERITY_OK;
  }

}
