<?php

namespace Drupal\keycdn\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * The KeycdnCacheTagHeaderGenerator class.
 */
class KeycdnCacheTagHeaderGenerator implements EventSubscriberInterface {

  /**
   * Generates a 'Cache-Tag' header in the format expected by KeyCDN.
   */
  public function onRespond(ResponseEvent $event) {
    if (!$event->isMainRequest()) {
      return;
    }

    $response = $event->getResponse();
    if (method_exists($response, 'getCacheableMetadata')) {
      $cache_metadata = $response->getCacheableMetadata();
      $cache_tags = $cache_metadata->getCacheTags();
      // If there are no X-Drupal-Cache-Tags headers, do nothing.
      if (!empty($cache_tags)) {
        $hashes = static::cacheTagsToHashes($cache_tags);
        $response->headers->set('Cache-Tag', implode(' ', $hashes));
        $response->headers->set('Content-Length', strlen($response->getContent()));
      }
    }
  }

  /**
   * Maps cache tags to hashes.
   *
   * Advantages:
   *  - Remove invalid chars like -
   *  - Reduces header size as KeyCDN limits at 8k.
   *
   * To see the plain text headers, enable Drupal's built in Header. See
   * https://www.drupal.org/node/2592471.
   *
   * @param string[] $cache_tags
   *   The cache tags in the header.
   *
   * @return string[]
   *   The hashes to use instead in the header.
   */
  public static function cacheTagsToHashes(array $cache_tags): array {
    $hashes = [];
    foreach ($cache_tags as $cache_tag) {
      $hashes[] = substr(md5($cache_tag), 0, 3);
    }
    return $hashes;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[KernelEvents::RESPONSE][] = ['onRespond'];
    return $events;
  }

}
