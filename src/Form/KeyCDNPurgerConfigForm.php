<?php

namespace Drupal\keycdn\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\keycdn\Entity\KeyCDNPurgerSettings;
use Drupal\purge_ui\Form\PurgerConfigFormBase;
use GuzzleHttp\ClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form for KeyCDN configurable purgers.
 */
class KeyCDNPurgerConfigForm extends PurgerConfigFormBase {

  /**
   * The http client.
   */
  protected ClientInterface $httpClient;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->httpClient = $container->get('http_client');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'keycdn.config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $settings = KeyCDNPurgerSettings::load($this->getId($form_state));
    $form['name'] = [
      '#title' => $this->t('Name'),
      '#type' => 'textfield',
      '#description' => $this->t('A label that describes this purger.'),
      '#default_value' => $settings->name,
      '#required' => TRUE,
    ];
    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#required' => TRUE,
      '#description' => $this->t('Enter the api key here.'),
      '#default_value' => $settings->api_key,
    ];
    $form['zone'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Zone'),
      '#required' => TRUE,
      '#description' => $this->t('Enter the zone id here.'),
      '#default_value' => $settings->zone,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $api_key = $form_state->getValue('api_key');
    $zone_id = $form_state->getValue('zone');
    $uri = 'https://api.keycdn.com/zones.json';

    // If api key and zone is set.
    if (!empty($api_key) && !empty($zone_id)) {
      try {
        $response = $this->httpClient->request('GET', $uri, [
          'auth' => [$api_key, ''],
        ]);

        // If key is not valid.
        if ($response->getStatusCode() != 200) {
          $form_state->setErrorByName('api_key', $this->t('Please check api key. It is either invalid or wrong.'));
        }

        // Validate the zone.
        $output = json_decode($response->getBody());
        if (empty($output->data->zones)) {
          $form_state->setErrorByName('zone', $this->t('Invalid zone id.'));
        }
        else {
          $valid_zone = FALSE;
          foreach ($output->data->zones as $zone) {
            // If zone id matches, no need to process the loop further.
            if ($zone->id == $zone_id) {
              $valid_zone = TRUE;
              break;
            }
          }

          // If not a valid zone.
          if (!$valid_zone) {
            $form_state->setErrorByName('zone', $this->t('Invalid zone id.'));
          }
        }
      }
      catch (\Exception $e) {
        $form_state->setErrorByName('api_key', $e->getMessage());
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitFormSuccess(array &$form, FormStateInterface $form_state) {
    $settings = KeyCDNPurgerSettings::load($this->getId($form_state));

    // Iterate the config object and overwrite values found in the form state.
    foreach ($settings as $key => $default_value) {
      if (!is_null($value = $form_state->getValue($key))) {
        $settings->$key = $value;
      }
    }
    $settings->save();
  }

}
