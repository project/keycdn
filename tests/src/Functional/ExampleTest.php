<?php

namespace Drupal\Tests\keycdn\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Test description.
 *
 * @group keycdn
 */
class ExampleTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['keycdn'];

  /**
   * Test callback.
   */
  public function testSomething() {
    // In Drupal 10.1 'access administration pages' was sufficient for /admin
    // But from 10.2 'administer site configuration' is also required.
    $admin_user = $this->drupalCreateUser([
      'access administration pages',
      'administer site configuration',
    ]);
    $this->drupalLogin($admin_user);
    $this->drupalGet('admin');
    $this->assertSession()->elementExists('xpath', '//h1[text() = "Administration"]');
  }

}
