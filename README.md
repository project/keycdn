Introduction
=======================

1. Sets the Cache-Tag http header required by KeyCDN for tag based Purging. No config is required for this.
1. Offers a purger plugin for invalidating via tag at KeyCDN. See below.

To enable purging, browse to admin/config/development/performance/purge and
add the purger provided by this module. To configure the purger, you need
a KeyCDN key and region name information. Visit https://www.keycdn.com/ to
get that info.

CI
==============
This module participates in the [DrupalCI alpha for Gitlab integration](https://www.drupal.org/project/infrastructure/issues/3265092).

As part of GitLab CI testing, there may be some unexpected or experimental pipeline configuration. 

Local Development
==============
A DDEV configuration is included for convenience.
